module.exports = () => {
  const fs = require('fs')
  const yaml = require('js-yaml')

  var referencesPrivate = "cv/references/private.yaml";
  var referencesPublic = "cv/references/public.yaml";
  var referencesFile;

  if (fs.existsSync(referencesPrivate)) {
    referencesFile = referencesPrivate;
  } else {
    referencesFile = referencesPublic;
  }

  references = yaml.load(fs.readFileSync(referencesFile, 'utf8'));
  return references;
}
