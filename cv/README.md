<!---------------------------------------------------------------------------->

# My CV

<!---------------------------------------------------------------------------->

The build and deployment of my CV is now integrated with my personal website.
The instructions below have been retained to demonstrate how to manually build
the CV locally without CI.

<!---------------------------------------------------------------------------->

## Prerequisites

To build this CV for viewing, the following prerequisites are required:

- [resume-cli]: v3.0.8 or greater
- [npm]: for installing [nodejs-yaml] and [JSON Resume] themes
- [js-yaml]: v4.1.0 or greater - see below
- A [JSON Resume] compatible theme - see below

<!---------------------------------------------------------------------------->

## YAML Parser

The references script within this CV requires a YAML parser to be present in
the `node_modules` folder. For this we are using: [js-yaml].

```
$ npm install js-yaml
```

<!---------------------------------------------------------------------------->

## Themes

Two themes that I commonly use are as follows:

* [jsonresume-theme-macchiato](https://github.com/biosan/jsonresume-theme-macchiato)
* [jsonresume-theme-stackoverflow](https://github.com/phoinixi/jsonresume-theme-stackoverflow)

They can be installed by using the below commands from the root of this
repository:

```
$ npm install jsonresume-theme-macchiato
$ npm install jsonresume-theme-stackoverflow
```

The themes and all required dependencies will now installed in the
`node_modules` folder. This folder is set to be ignored by the `.gitignore`
file.

Additional themes can be installed by running `npm install` followed by the
name of the theme you would like to install. Same as before, this should be ran
from the root of this repository.

<!---------------------------------------------------------------------------->

## Validating My CV

Check that all inputs are valid by running the following:

```
$ resume validate --resume "cv/source"
```

No output from this command is good!

<!---------------------------------------------------------------------------->

## Building My CV

To generate a PDF version of the CV, run the following:

```
$ resume export --resume "cv/source" --theme "jsonresume-theme-stackoverflow" --format pdf "cv/cv.pdf"
```

This will produce my CV with:
- Limited contact information
- Public references
- Using theme **jsonresume-theme-stackoverflow**
- Output to the **cv/** folder, file: **cv.pdf**

**NOTE**: The theme specified by `--theme` must have been installed by using
the steps above.

<!---------------------------------------------------------------------------->

## Prebuilt CVs

* Macchiato Theme: [cv-macchiato.html]
* Stack Overflow Theme: [cv-stackoverflow.html]

For a CV complete with contact information and actual referees please contact
me and I would be happy to provide one.

<!---------------------------------------------------------------------------->

[npm]:         https://www.npmjs.com/
[js-yaml]:     https://www.npmjs.com/package/js-yaml
[resume-cli]:  https://github.com/jsonresume/resume-cli
[JSON Resume]: https://jsonresume.org/

<!---------------------------------------------------------------------------->

[cv-macchiato.html]:     https://rich.bitservices.io/cv/cv-macchiato.html
[cv-stackoverflow.html]: https://rich.bitservices.io/cv/cv-stackoverflow.html

<!---------------------------------------------------------------------------->
